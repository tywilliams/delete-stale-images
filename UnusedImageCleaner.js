const fs = require('fs');
const path = require('path');

const isAnImage = (filePath) => {
    const extension = path.extname(filePath);
    return extension === '.png' || extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.svg';
}

const UnusedImageCleaner = {
    // Recursively check the a directory for image files and return their name.
    getSourceImages: function (directory, trimPrefix = false) {
        const sourceImages = [];
        const files = fs.readdirSync(directory);
        files.forEach(file => {
            const filePath = path.join(directory, file);
            const stats = fs.statSync(filePath);
            if (stats.isDirectory()) {
                sourceImages.push(...UnusedImageCleaner.getSourceImages(filePath));
            } else if (isAnImage(filePath)) {
                sourceImages.push(filePath);
            }
        });

        if (trimPrefix) {
            return sourceImages.map(image => image.replace('source/', ''));
        }

        return sourceImages;
    },

    // Check a file for image references and return their names in an array
    getImageReferencesFromFileContents: function (filePath) {
        const fileContents = fs.readFileSync(filePath, 'utf8');
        return fileContents.match(/images\/.*\.(jpg|jpeg|png|gif|svg)/g);
    },

    // Recursively read every file in the directory, and return all references to images.
    getImageReferences: function (directory, ignoreList = []) {
        const imageReferences = [];
        const files = fs.readdirSync(directory);
        files.forEach(file => {
            const filePath = path.join(directory, file);
            // Skip if the filePath is in the ignorelist
            if (ignoreList.includes(filePath)) {
                return;
            }

            const stats = fs.statSync(filePath);
            if (stats.isDirectory()) {
                imageReferences.push(...UnusedImageCleaner.getImageReferences(filePath));
            } else {
                const references = UnusedImageCleaner.getImageReferencesFromFileContents(filePath);
                // Add the contents of the references array to imageReferences
                if (references) {
                    imageReferences.push(...references);
                }
            }
        });

        return imageReferences;
    },

    getUnusedImages: function (sourceImages, imageReferences) {
        // Create an object from sourceImagesObject, where each key is the image path and the value is 0
        const sourceImagesObject = {};
        sourceImages.forEach(function (image) {
            sourceImagesObject[image] = 0;
        });

        // Loop through imageReferences, and increment the value of the image path in sourceImagesObject
        imageReferences.forEach(function (imageReference) {
            if (sourceImagesObject.hasOwnProperty(imageReference)) {
                sourceImagesObject[imageReference]++;
            }
        });

        // Create an array of image paths that have a value of 0
        const unusedImages = [];
        for (let image in sourceImagesObject) {
            if (sourceImagesObject[image] === 0) {
                unusedImages.push(image);
            }
        }

        return unusedImages;
    }
}

module.exports = UnusedImageCleaner;