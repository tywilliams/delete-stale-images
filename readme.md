# Stale File Checker

This is a demo repo for a little JavaScript utility that can check repositories for images that aren't used in source code. 

## Local set up

1. Clone the repo
1. `yarn install`
1. `yarn test`

This will run the tests that check the functionality against the `images` and `src` folder of this repository. 

## Usage elsewhere

1. Make sure your project has [Node.js](https://nodejs.org/en/) installed. 
1. Grab the code from `UnusedImageCleaner.js`
1. Paste it into a file called `UnusedImageCleaner.js` in the root of your project
1. Update the arguments to `UnusedImageCleaner.findImageFilesInDirectory` and `UnusedImageCleaner.findReferencesToImagePaths`
1. Run `node UnusedImageCleaner.js` in the repository
1. The script should create a text file called `unusedImages.txt` with the unused images.
1. Do whatever you want with that information, perhaps like `git rm` or something. 
1. Modify `UnusedImageCleaner.js` as needed.