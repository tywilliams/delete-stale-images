const UnusedImageCleaner = require('./UnusedImageCleaner');

test('Finding source images', () => {
    const expected = [
        'source/images/default-blog-image.png',
        'source/images/devops-tools/gitlab-logo.svg',
        'source/images/gogs-logo.png',
        'source/images/home/forrester-vsm-graphic.png',
        'source/images/jvm_ecosystem_report_2020.png',
    ]

    const sourceImages = UnusedImageCleaner.getSourceImages('source/images');
    expect(sourceImages).toEqual(expected);
});

test('Trimming source/ from images', () => {
    const expected = [
        'images/default-blog-image.png',
        'images/devops-tools/gitlab-logo.svg',
        'images/gogs-logo.png',
        'images/home/forrester-vsm-graphic.png',
        'images/jvm_ecosystem_report_2020.png',
    ]

    const sourceImages = UnusedImageCleaner.getSourceImages('source/images', true);
    expect(sourceImages).toEqual(expected);
})

test('Finding image paths in a file', () => {
    const expected = [
        'images/home/icons-pattern-left.svg',
        'images/home/icons-pattern-right.svg',
        'images/devops-tools/gitlab-logo.svg',
    ]

    const imagesInFile = UnusedImageCleaner.getImageReferencesFromFileContents('./sites/uncategorized/source/analysts/index.html.haml');
    expect(imagesInFile).toEqual(expected);
})

test('Finding all references to images', () => {
    const expected = [
        'images/default-blog-image.png',
        'images/devops-tools/gitlab-logo.svg',
        'images/home/forrester-vsm-graphic.png',
        'images/jvm_ecosystem_report_2020.png',
    ]

    const imageReferences = UnusedImageCleaner.getImageReferences(__dirname);
    // Expect that we will at least have the values in expected
    expect(imageReferences).toContain(expected[0]);
    expect(imageReferences).toContain(expected[1]);
    expect(imageReferences).toContain(expected[2]);
    expect(imageReferences).toContain(expected[3]);
})

test('Checking which images are not used', () => {
    const expected = ['images/gogs-logo.png']
    const sourceImages = UnusedImageCleaner.getSourceImages('source/images', true);
    const imageReferences = UnusedImageCleaner.getImageReferences(__dirname, [`${__dirname}/UnusedImageCleaner.test.js`]);
    const unusedImages = UnusedImageCleaner.getUnusedImages(sourceImages, imageReferences);

    expect(unusedImages).toEqual(expected);
})